let body = document.querySelector('body');
let images, currentImage;

$( document).ready(function(){
  document.querySelectorAll('.wk-projectGallery__Container').forEach(function(el) {
    el.querySelectorAll('.wk-projectGallery__Thumbnail').forEach(function(el){
      el.addEventListener('click', function(){
        createGallery(this.dataset.gallery, this.dataset.index);
      });
    });
  });
});

function createGallery(name, index) {
  let doc = $.getJSON(`/galleries/${name}.json`, function(data){
    images = [];
    currentImage = Number(index);
    let fragment = document.createDocumentFragment();
    let show = document.createElement('div');
    show.className = 'wk-projectShow';
    let container = document.createElement('div');
    container.className = 'wk-projectShow__imagesContainer';
    data.files.forEach( function(file){
      let image = document.createElement('img');
      image.className = 'wk-projectShow__image';
      image.setAttribute('src', `/img/work/${name}/${file}`);
      container.appendChild(image);
      images.push(image);
    });

    let closeIcon = document.createElement('i');
    closeIcon.className = 'icon-cancel-circled wk-projectShow__close';
    closeIcon.addEventListener('click', () => $(`.wk-projectShow`).remove());

    let leftArrow = document.createElement('i');
    leftArrow.className = 'icon-angle-left';
    leftArrow.addEventListener('click', function() {
    currentImage = (currentImage == 0)
        ? images.length - 1
        : currentImage - 1
      $('.wk-projectShow__imagesContainer').scrollTo(images[currentImage], 400);
    });

    let rightArrow = document.createElement('i');
    rightArrow.className = 'icon-angle-right';
    rightArrow.addEventListener('click', function() {
      currentImage = (currentImage == images.length - 1)
        ?  0
        :  currentImage + 1
      $('.wk-projectShow__imagesContainer').scrollTo(images[currentImage], 400);
    });

    show.appendChild(closeIcon);
    show.appendChild(container);
    show.appendChild(leftArrow);
    show.appendChild(rightArrow);
    fragment.appendChild(show);
    images[currentImage].onload = function(){
      body.appendChild( fragment);
      images[currentImage].scrollIntoView();
    };
  });
}

